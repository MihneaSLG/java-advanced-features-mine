import solid.Car;
import solid.ElectricCar;
import solid.MotorCar;

public class Main {
    public static Car car;
    public static void drive(Car car){
        car.start();
        car.accelerate(10);
        car.stop();
    }
    public static void main(String[] args) {
        System.out.println("Yes");


        car = new MotorCar();
        Car electricCar = new ElectricCar();
        drive(electricCar);
        System.out.println("****************");
        ElectricCar carSpecific = new ElectricCar();
        specificDrive(carSpecific);
        drive(carSpecific);
    }
    public static void specificDrive(ElectricCar electricCar){
        electricCar.start();;
        electricCar.accelerate(45);
        electricCar.stop();
        electricCar.regeneratingBrake();
    }

}

