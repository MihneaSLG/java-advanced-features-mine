package designPatterns.factories;

import designPatterns.pizza.Pizza;
import designPatterns.pizza.PizzaType;

public interface AbstractFactory {
    Pizza create(PizzaType type, int size);



}
