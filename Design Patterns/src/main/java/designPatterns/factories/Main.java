package designPatterns.factories;

import designPatterns.pizza.Pizza;
import designPatterns.pizza.PizzaType;

public class Main {
    public static void main(String[] args) {
        AbstractFactory pizzaFactory = new PizzaFactory();
        Pizza comanda1 = pizzaFactory.create(PizzaType.QUATROFORMAGGI, 30);
        Pizza comanda2 = pizzaFactory.create(PizzaType.MARGHERITA, 50);
        Pizza comanda3 = pizzaFactory.create(PizzaType.QUATROSTASTAGIONNI, 35);
        System.out.println(comanda1);
        System.out.println(comanda2);
        System.out.println(comanda3);

    }
}
