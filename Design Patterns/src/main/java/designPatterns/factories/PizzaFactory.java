package designPatterns.factories;

import designPatterns.pizza.*;

public class PizzaFactory implements AbstractFactory{
    @Override
    public Pizza create(PizzaType type, int size) {
        switch (type){
            case MARGHERITA:
                return new PizzaMargherita(size);
                case QUATROFORMAGGI:
                    return new PizzaQuatroformagi(size);
                case QUATROSTASTAGIONNI:
                    return new PizzaQuatrostagionni(size);
            default: return null;

        }

    }
}
