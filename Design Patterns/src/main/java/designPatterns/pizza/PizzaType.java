package designPatterns.pizza;

public enum PizzaType {
    QUATROSTASTAGIONNI, MARGHERITA, QUATROFORMAGGI
}
