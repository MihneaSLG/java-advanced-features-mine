package designPatterns.pizza;

public class PizzaQuatrostagionni extends Pizza {
    private int size;

    public PizzaQuatrostagionni(int size){
        this.size = size;

    }
    @Override
    public String getName() {
        return PizzaType.QUATROSTASTAGIONNI.toString();
    }

    @Override
    public String getIngredients() {
        return "blat, mozarella, sos rosii, ciuperci, salam";
    }

    @Override
    public int getSize() {
        return size ;
    }
}
