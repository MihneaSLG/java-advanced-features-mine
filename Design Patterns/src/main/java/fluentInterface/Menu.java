package fluentInterface;

import java.util.List;

public interface Menu {
    public Menu orderPizza(List<String> orders);
    public Menu eatPizza();
    public Menu payPizza();
}
