package fluentInterface;


import java.util.ArrayList;
import java.util.List;

public class foodMenu implements Menu {
    List<PizzaQuatroStagionni> pizzalist;
    List<String> selectedPizza;

    public foodMenu() {
        this.pizzalist = new ArrayList<>();
        populateMenu();
        this.selectedPizza = new ArrayList<>();
        populateMenu();
    }

    private void populateMenu() {
        Pizza pizza = new PizzaQuatroStagionni();
        pizzalist.add((PizzaQuatroStagionni) pizza);



    }

    public Menu orderPizza(List<String> orders) {
        for (String order:orders){
            selectedPizza.add(order);
            System.out.println("I have ordered pizza" + " " +order);
        }
        return this;
    }

    public Menu payPizza() {
        System.out.println("I have payed for the pizza");
        return this;
    }

    public Menu eatPizza() {
        System.out.println("I have eaten the pizza");
        return this;
    }
}

