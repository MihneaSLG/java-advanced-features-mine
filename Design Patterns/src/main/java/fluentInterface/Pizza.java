package fluentInterface;

public interface Pizza {
    public Pizza getName();
    public Pizza getIngredients();
    public Pizza getCost();

}
