package Recapitulare;

public class Main {
    public static void main(String[] args) {
        Person man1 = new Student("Masterand", 1,5000,"Andrei","Bucuresti, Romania");
        Person man2= new Staff("Cristi", "Timisoara", "Medicina",60000);
        System.out.println(man1);
        System.out.println(man2);

        Person student = new Student("Student",2,4500);
        Person staff = new Staff("farmacie", 5300);
        System.out.println(student);
        System.out.println(staff);

        student.setName("Matei");
        student.setAddress("Cluj");
        staff.setName("Rares");
        staff.setAddress("Suceava");
        System.out.println(student);
        System.out.println(staff);

        staff.f();
    }

}




