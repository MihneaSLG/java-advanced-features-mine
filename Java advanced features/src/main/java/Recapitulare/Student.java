package Recapitulare;

public class Student extends Person  {
    public String typeOfStudy;
    public int yearOfStudy;
    public float studyPrice;

    @Override
    public void f(){
        System.out.println("Sunt studentul" + name);
    }

    public Student( String typeOfStudy, int yearOfStudy, float studyPrice, String name, String address) {
        super(name, address);
        this.typeOfStudy = typeOfStudy;
        this.yearOfStudy = yearOfStudy;
        this.studyPrice = studyPrice;
    }

    public Student(String typeOfStudy, int yearOfStudy, float studyPrice){
        super();
        this.typeOfStudy = typeOfStudy;
        this.yearOfStudy = yearOfStudy;
        this.studyPrice = studyPrice;
    }


    public String getTypeOfStudy() {
        return typeOfStudy;
    }

    public void setTypeOfStudy(String typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public float getStudyPrice() {
        return studyPrice;
    }

    public void setStudyPrice(float studyPrice) {
        this.studyPrice = studyPrice;
    }

    @Override
    public String toString() {
        return "Student{" +
                "typeOfStudy='" + typeOfStudy + '\'' +
                ", yearOfStudy=" + yearOfStudy +
                ", studyPrice=" + studyPrice +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}

