package Maps;

import java.util.HashMap;
import java.util.Map;

public class Main2 {
    public static void main(String[] args) {
        Map<String, Integer> students1 = new HashMap<>();
        students1.put("George", 9);
        students1.put("Paul", 7);
        students1.put("Ioana", 4);
        students1.put("Laura", 10);
        students1.put("Andrei", 2);

        for(Map.Entry<String, Integer>student: students1.entrySet()){
            System.out.println(student);
            String key = student.getKey();
            Integer value = student.getValue();
            System.out.printf("%s : %d  \n", key,value );
        }
        for(Integer nota : students1.values()){
            System.out.println(nota);
        }

        System.out.println("Nota Ioanei" + ":" + " " + students1.get("Ioana"));
    }
}
